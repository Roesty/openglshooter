#ifndef Game_H
#define Game_H

#include <iostream>
#include <GL/glew.h>
#include <GL/gl.h>
#include <SDL2/SDL.h>
#include <stdio.h>
#include <memory>

#include "world.h"
#include "renderer.h"

class Game
{
public:
    Game();
    ~Game();

    bool init();
    bool startGame();

private:
    const GLuint FPS = 60;
    const GLuint TICKS_PER_FRAME = 1000 / FPS;

    //number of ticks passed when the game started
    GLuint m_startTick;

    //number of ticks that has passed since the game started
    GLuint elapsedTicks();

    //number of frames that has passed in the game
    GLuint m_elapsedFrames;

    //Pointer to the SDL2 keyboard states
    const Uint8* keyboardState;

    //the world object
    //handles every object in the world
    //the world object is shared with the renderer
    std::shared_ptr<World> m_world;

    //the renderer object
    //handles the window as well as rendering of all objects
    std::unique_ptr<Renderer> m_renderer;

    /////////////////////////// REMOVE REMOVE REMOVE
    GLfloat camx;
    GLfloat camy;
    GLfloat camz;
    GLfloat camzMax;
    //////////////////////////

    //true and keeps the main game loop running
    bool isRunning;

    //after init has finished, this sets up the final pieces for the game and starts it
    bool start();

    //the main game loop handles input, game logic and rendering
    //TODO: Split up into readInput, handleInput, render
    bool run();

    //if the user exits, this cleans up the program
    bool stop();

};

#endif // Game_H
