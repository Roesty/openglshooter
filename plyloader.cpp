#include "plyloader.h"

using namespace std;

PlyLoader::PlyLoader()
{

}

string PlyLoader::getError()
{
    return m_error;
}

bool PlyLoader::loadFile(std::string fileName)
{
    glm::vec3 vertices;
    glm::vec3 colors;

    string line;

    ifstream plyFile (fileName);

    unsigned int expectedVertices=0;
    unsigned int expectedFaces=0;

    if (plyFile.is_open())
    {
        getline(plyFile, line);
        if (line!="ply")
        {
            m_error="Not valid ply file.";
            return true;
        }

        while (getline(plyFile, line))
        {
            vector<string> tokens;

            while (!line.empty())
            {
                cout<<line<<endl;

                int position = line.find_first_of(' ');

                if (position == -1)
                {
                    tokens.push_back(line);
                    line.clear();
                }
                else
                {
                    tokens.push_back(line.substr(0, position));
                    line = line.erase(0, position+1);
                }
            }

            //Skip this line if the first token is not of interest
            if (tokens[0] == "format" || tokens[0] == "comment")
            {
                continue;
            }

            //Iterate all the tokens to see what is there
            for (unsigned int iii=0; iii<tokens.size(); iii++)
            {
                cout<<tokens[iii]<<", ";
            }
            cout<<endl;
        }
        plyFile.close();
    }
    else
    {
        m_error="Could not open file '"+fileName+"'.";
    }

    return false;
}
























































