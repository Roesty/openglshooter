#ifndef MODEL3DCUSTOM_H
#define MODEL3DCUSTOM_H

#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <stb_image.h>

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <map>
#include <vector>

#include "mesh.h"

using namespace std;

class Model3dCUSTOM
{
public:
    Model3dCUSTOM();

    void giveVertex(SimpleVertex newVertex);

    //Sends all vertex data to OpenGL for storage
    //Only call after all vertices has been given
    void store();

    GLuint VAO() const;
    GLuint VBO() const;
    GLuint EBO() const;

    GLuint verticesSize() const;

private:
    vector<SimpleVertex> m_vertices;
    vector<unsigned int> m_indices;

    GLuint m_VAO;
    GLuint m_VBO;
    GLuint m_EBO;
};

#endif // MODEL3DCUSTOM_H
