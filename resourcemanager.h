#ifndef RESOURCEMANAGER_H
#define RESOURCEMANAGER_H

//https://www.khronos.org/collada/

#include <iostream>
#include <string>

#include <memory>
#include <vector>

#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>

#include "model3d.h"
#include "objloader.h"
#include "plyloader.h"

enum eModels {
    CUBE=0,
    PYRAMID,
    DIAMOND,
    DODECAHEDRON,
    ICOSAHEDRON,
    OCTAHEDRON,
    CRATE,
    GRAY_BOX,
    BULLET,
    BEAR,
    DOG,
    PISTOL_AMMO_BOX,
    PLAIN_MAN,
    BACKPACK,
    HANDGUN_45ACP,
    HOUSE,
    HOUSE2,
    CITY,
    GOURD,
    SWORD,

    MAX_MODELS
};

class ResourceManager
{
public:
    ResourceManager();

    //Creates references to every model in models3d directory
    void start();

    //Unloads all models
    void stop();

    //Unloads all models with a reference count of 1
    void clean();

    //Returns a pointer to the model that has been assigned to this ID
    std::shared_ptr<Model3dCUSTOM> getModel(eModels model);

    //Returns a pointer to the model at the path if it exists
    std::shared_ptr<Model3dCUSTOM> getModel(std::string path);

private:
    //    const std::string RESOURCE_DIRECTORY="/path/to/resources/";
    const std::string RESOURCE_DIRECTORY="/home/lace/QtProjects/3dShooter/3dShooter/resources/";
    const std::string SHADER_DIRECTORY=RESOURCE_DIRECTORY+"shaders/";
    const std::string TEXTURE_DIRECTORY=RESOURCE_DIRECTORY+"textures/";
    const std::string MODELS3D_DIRECTORY=RESOURCE_DIRECTORY+"models3d/";
    const std::string MODELS3D_OBJ_DIRECTORY=MODELS3D_DIRECTORY+"obj/";
    const std::string MODELS3D_PLY_DIRECTORY=MODELS3D_DIRECTORY+"ply/";

    std::shared_ptr<Model3dCUSTOM> load(std::string path);
    void unload(std::string path);

    void createReferences(std::string path);

    struct ModelReference
    {
        std::string path;
        std::shared_ptr<Model3dCUSTOM> reference;
    };

    std::vector<ModelReference> m_models;

    ObjLoader m_objLoader;
    PlyLoader m_plyLoader;

};

#endif // RESOURCEMANAGER_H




































