#include "object3d.h"

Object3d::Object3d(std::string name)
{
    m_name=name;
//    SDL_Log("Constructing %s", m_name.c_str());

    m_parent.reset();
    m_model.reset(nullptr);
    m_type=ENTITY_OBJECT;
    m_position=glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
    m_rotation=glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);
    m_scale=glm::vec4(1.0f, 1.0f, 1.0f, 0.0f);

    m_trans = glm::mat4(1.0f);
    m_trans = glm::rotate(m_trans, glm::radians(90.0f), glm::vec3(0.0, 0.0, 1.0));
    m_trans = glm::scale(m_trans, glm::vec3(0.5, 0.5, 0.5));
}

//Copy constructor
Object3d::Object3d(const Object3d &other)
{
    m_name=other.m_name+" COPY";
//    SDL_Log("Constructing (COPYING) %s", m_name.c_str());

    m_parent=nullptr;

    m_type=other.m_type;

    m_position=other.m_position;
    m_rotation=other.m_rotation;
    m_scale=other.m_scale;
}

Object3d::~Object3d()
{
//    SDL_Log("Deleting Object3d '%s'", getName().c_str());
}

void Object3d::translate(glm::vec3 vector)
{
    glm::mat4 translationMatrix = glm::translate(glm::mat4(1.0f), vector);
    m_position = translationMatrix*m_position;
}

void Object3d::rotate(glm::vec3 vector)
{
    (void) vector;
    determineDirections();
}

void Object3d::scale(glm::vec3 vector)
{
    glm::mat4 scalingMatrix = glm::scale(vector);
    m_scale = scalingMatrix*m_scale;
}

glm::vec4 Object3d::getPosition(eScope scope)
{
    switch (scope)
    {
    case SCOPE_LOCAL:
    {
        return m_position;
    }
    case SCOPE_GLOBAL:
    {
        if (m_parent)
        {
            glm::vec3 translationVector(m_parent->getPosition(SCOPE_GLOBAL));
            glm::mat4 translationMatrix = glm::translate(glm::mat4(1.0f), translationVector);
            return translationMatrix*m_position;
        }
        return m_position;
    }
    }
}

glm::vec4 Object3d::getRotation(eScope scope)
{
    return m_rotation;
    (void) scope;
    //return glm::rotate(glm::mat4(1.0f), 0.0f, glm::vec3(0.0f, 0.0f, 0.0f));
    return glm::vec4(1.0f);
}

glm::vec4 Object3d::getScale(eScope scope)
{
    switch (scope)
    {
    case SCOPE_LOCAL:
    {
        return m_scale;
    }
    case SCOPE_GLOBAL:
    {
        if (m_parent)
        {
            glm::vec3 scalingVector(m_parent->getScale(SCOPE_GLOBAL));
            glm::mat4 scalingMatrix = glm::scale(glm::mat4(1.0f), scalingVector);
            return scalingMatrix*m_scale;
        }
        return m_scale;
    }
    }
}

void Object3d::setPosition(glm::vec3 position)
{
    m_position=glm::vec4(position, 1.0f);
}

void Object3d::setRotation(glm::vec3 rotation)
{
    m_rotation=glm::vec4(rotation, 0.0f);
    determineDirections();
}

void Object3d::setScale(glm::vec3 scale)
{
    m_scale=glm::vec4(scale, 0.0f);
}

bool Object3d::hasModel()
{
    return m_model.get()!=nullptr;
}

std::string Object3d::getName() const
{
    return m_name;
}

void Object3d::setName(const std::string &name)
{
    m_name = name;
}

 std::shared_ptr<Object3d> Object3d::getParent() const
{
    return m_parent;
}

void Object3d::setParent( std::shared_ptr<Object3d> parent)
{
    m_parent=parent;
}

Model3d Object3d::getModel() const
{
    //std::cout<<"Model: "<<m_model.get()<<std::endl;
    return *getModelPtr();
}

Model3d* Object3d::getModelPtr() const
{
    //std::cout<<"Model: "<<m_model.get()<<std::endl;
    return m_model.get();
}

void Object3d::setType(eEntity type)
{
    m_type=type;
}

eEntity Object3d::getType() const
{
    return m_type;
}

void Object3d::setModel(eModels objectTemplate)
{
    std::string objectPath="obj/";
    switch (objectTemplate) {

    case CUBE:
    {
        objectPath+="generics/cube.obj";
        break;
    }

    case PYRAMID:
    {
        objectPath+="generics/pyramid.obj";
        break;
    }

    case CRATE:
    {
        objectPath+="crates/Crates.obj";
        break;
    }

    case GRAY_BOX:
    {
        objectPath+="grayBox/boxs.obj";
        break;
    }

    case BULLET:
    {
        break;
    }

    case HOUSE:
    {
        objectPath+="house/house1.obj";
        break;
    }

    case HOUSE2:
    {
        objectPath+="house2/house_obj.obj";
        break;
    }

    case PISTOL_AMMO_BOX:
    {
        objectPath+="pistolAmmoBox/box.obj";
        break;
    }

    case PLAIN_MAN:
    {
        objectPath+="plainMan/FinalBaseMesh.obj";
        break;
    }

    case BACKPACK:
    {
        objectPath+="backpack/backpack.obj";
        break;
    }

    case HANDGUN_45ACP:
    {
        objectPath+="45ACP/Handgun_obj.obj";
        break;
    }

    case MAX_MODELS:
    {
        break;
    }

    }

    m_model.reset(new Model3d(objectPath));
}

void Object3d::setModel(std::shared_ptr<Model3dCUSTOM> model)
{
    SDL_Log("Gave model");
    m_modelCUSTOM=model;
}

glm::mat4 Object3d::getTrans() const
{
    return m_trans;
}

void Object3d::setTrans(const glm::mat4 &trans)
{
    m_trans = trans;
}

std::shared_ptr<Model3dCUSTOM> Object3d::getModelCUSTOM() const
{
    return m_modelCUSTOM;
}

glm::vec3 Object3d::getForward() const
{
    return m_forward;
}

glm::vec3 Object3d::getRight() const
{
    return m_right;
}

glm::vec3 Object3d::getUp() const
{
    return m_up;
}

void Object3d::determineDirections()
{
    m_right=glm::normalize(glm::cross(GLOBAL_UP, glm::vec3(getRotation())));
    m_up=glm::cross(glm::vec3(getRotation()), m_right);
}













































