#include "world.h"

World::World()
{
    m_entities.clear();
    m_resourceManager.start();
}

World::~World()
{
}

void World::testWorld()
{
    std::vector<glm::vec3> cratePositions;
    cratePositions.push_back(glm::vec3( 0.0f,  0.0f,  -5.0f));
    cratePositions.push_back(glm::vec3( 2.0f,  5.0f, -15.0f));
    cratePositions.push_back(glm::vec3(-1.5f, -2.2f, -2.5f));
    cratePositions.push_back(glm::vec3(-3.8f, -7.0f, -12.3f));
    cratePositions.push_back(glm::vec3( 2.4f, -0.4f, -3.5f));
    cratePositions.push_back(glm::vec3(-1.7f,  3.0f, -7.5f));
    cratePositions.push_back(glm::vec3( 1.3f, -2.0f, -2.5f));
    cratePositions.push_back(glm::vec3( 1.5f,  2.0f, -2.5f));
    cratePositions.push_back(glm::vec3( 1.5f,  0.2f, -1.5f));
    cratePositions.push_back(glm::vec3(-1.3f,  1.0f, -1.5f));

    //Use the square as it is, default rotation and position
    //    SDL_Log("########## GENERATING WORLD ##########");

    for (GLuint iii=0; iii<cratePositions.size(); iii++)
    {
        std::string name = "CUBE "+std::to_string(iii);
        insertObject(new Object3d(name));
        m_entities.back()->setModel(PLAIN_MAN);
        m_entities.back()->translate(cratePositions[iii]);
    }

    //Create a camera
    insertObject(new Object3d("MAIN CAMERA"));
    m_entities.back()->setType(ENTITY_CAMERA);
    //Move it backwards on the z axis and point it towards the square
    m_entities.back()->translate(glm::vec3(0.0f, 0.0f, -5.0f));

    //    printContents();

    //    SDL_Log("########## DONE GENERATING WORLD ##########");
}

void World::testWorld2()
{
    std::vector<glm::vec3> cratePositions;
    glm::vec3 floorScale(5.0f, 0.5f, 5.0f);

    glm::vec3 size(1, 1, 1);
    size.x = 150;
    size.y = 1;
    size.z = 150;

    for (int x=0; x<size.x; x++)
        for (int y=0; y<size.y; y++)
            for (int z=0; z<size.z; z++)
            {
                cratePositions.push_back(glm::vec3(x*floorScale.x,
                                                   y*floorScale.y-20,
                                                   z*floorScale.z));
                //                cratePositions.push_back(glm::vec3(x*1.5f, y*1.5f, z*1.5f));
                //                cratePositions.push_back(glm::vec3(x*3.0f, y*3.0f, z*3.0f));
            }

    //Use the square as it is, default rotation and position
    SDL_Log("########## GENERATING WORLD ##########");

    for (unsigned int iii=0; iii<cratePositions.size(); iii++)
    {
        std::string name = "CUBE "+std::to_string(iii);
        insertObject(new Object3d(name));
        last()->scale(floorScale);
        //        m_entities.back()->setModel(CUBE);
        last()->setModel(m_resourceManager.getModel(CUBE));
        last()->translate(cratePositions[iii]);
    }

    insertObject("PYRAMID");
    last()->setModel(m_resourceManager.getModel(PYRAMID));
//    last()->setScale(glm::vec3(10, 10, 10));

    /*
    insertObject("DOG");
    last()->setModel(m_resourceManager.getModel(DOG));
    last()->translate(glm::vec3(20, 0, 0));
*/

    //Create a camera
    insertObject(new Object3d("MAIN CAMERA"));
    last()->setType(ENTITY_CAMERA);
    //Move it backwards on the z axis and point it towards the square
    last()->translate(glm::vec3(0.0f, 0.0f, -5.0f));

    //Create a cube that follows the camera
    /*
    insertObject("WEAPON");
    last()->setModel(m_resourceManager.getModel(CUBE));
    last()->setParent(getFirstCamera());
    last()->setScale(glm::vec3(0.1, 0.1, 0.1));
*/

    //printContents();

    SDL_Log("########## DONE GENERATING WORLD ##########");
}

std::shared_ptr<Object3d> World::last()
{
    return m_entities.back();
}

void World::printContents()
{
    SDL_Log("########## WORLD CONTENT ##########");
    SDL_Log("Total: %lu", m_entities.size());
    for (GLuint iii=0; iii<m_entities.size(); iii++)
    {
        if (iii==0)
            SDL_Log("Names:");

        Object3d* pointer = m_entities[iii].get();

        if (pointer)
        {
            SDL_Log("#%i: %s %s", iii, pointer->getName().c_str(), glm::to_string(pointer->getPosition()).c_str());
        }
        else
        {
            SDL_Log("NULL POINTER");
        }
    }

    SDL_Log("########## END OF CONTENT ##########");
}

std::vector<std::shared_ptr<Object3d> >* World::getEntitiesReference()
{
    return &m_entities;
}

std::shared_ptr<Object3d> World::getFirstCamera()
{
    for (GLuint iii=0; iii<m_entities.size(); iii++)
    {
        if (m_entities[iii]->getType()==ENTITY_CAMERA)
        {
            //Return a pointer to this transform

            SDL_Log("DETECTION %s", glm::to_string(m_entities[iii]->getPosition()).c_str());
            return m_entities[iii];
        }
    }

    //If no camera was found, return a null pointer
    return nullptr;
}

void World::generateTemplates()
{
    //CUBE
    Object3d cube;



}

void World::insertObject(Object3d* newObject)
{
    SDL_Log("Inserting %s", newObject->getName().c_str());
    std::shared_ptr<Object3d> shared_object(newObject);

    //    entities.push_back(std::move(shared_object));
    m_entities.push_back(std::move(shared_object));
}

void World::insertObject(std::string name)
{
    insertObject(new Object3d(name));
}



















