#ifndef OBJLOADER_H
#define OBJLOADER_H

//Good site: https://people.sc.fsu.edu/~jburkardt/data/obj/obj.html

#define GLM_ENABLE_EXPERIMENTAL

#include <fstream>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/ext.hpp>
#include <glm/gtx/string_cast.hpp>

#include <GL/glew.h>
#include <GL/gl.h>

#include <iostream>
#include <string>
#include <vector>
#include <memory>

#include "model3d.h"
#include "model3dcustom.h"

class ObjLoader
{
public:
    ObjLoader();

    std::shared_ptr<Model3d> loadFile(std::string fileName);
    std::shared_ptr<Model3dCUSTOM> loadFileCUSTOM(std::string fileName);
    std::string getError();

private:
    std::string m_error;

    std::vector<std::string> splitString(std::string inputString, std::string delimiter=" ");

    struct lVertex
    {
        float x, y, z;
        float r, g, b;
//        glm::vec3 position;
    };

    struct lColor
    {
        glm::vec3 color;
    };

    struct lTextureCoord
    {
        float u, v, w;
//        glm::vec3 coords;
    };

    struct lVertexNormal
    {
        float x, y, z;
//        glm::vec3 direction;
    };

    struct lFace
    {
        std::vector<unsigned int> vertices;
        std::vector<unsigned int> textureCoords;
        std::vector<unsigned int> vertexNormals;
    };

    bool m_colorDetected;

    std::vector<lVertex> m_vertices;
    std::vector<lTextureCoord> m_textureCoords;
    std::vector<lVertexNormal> m_vertexNormals;
    std::vector<lFace> m_faces;

};

#endif // OBJLOADER_H
