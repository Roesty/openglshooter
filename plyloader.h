#ifndef PLYLOADER_H
#define PLYLOADER_H

#include <fstream>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <iostream>
#include <string>
#include <vector>

struct Vertex2
{
    // position
    glm::vec3 position;

    // color
    glm::vec3 color;
};

class PlyLoader
{
public:
    PlyLoader();

    bool loadFile(std::string fileName);

    std::string getError();


private:

    std:: string m_error;
};

#endif // PLYLOADER_H
