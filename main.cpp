#include <iostream>

#include "game.h"
#include "plyloader.h"
#include "objloader.h"
#include "resourcemanager.h"

GLint main(GLint argc, char **argv)
{
    using namespace std;

    (void) argc;
    (void) argv;

    /*
    cout<<"------------------------------------------------------------------------"<<endl;
    cout<<"Game code heavily inspired by the excellent tutorials at learnopengl.com"<<endl;
    cout<<"------------------------------------------------------------------------"<<endl;
*/


    Game game;

    //Initialise the game
    if (game.init())
    {
        cout<<"Initialisation failed."<<endl;
        return 1;
    }
    cout<<"Initialisation succeeded."<<endl;

    //Start the game
    if (game.startGame())
    {
        cout<<"Game did not exit correctly."<<endl;
        return 2;
    }
    cout<<"Game did exit correctly."<<endl;

    return 0;
}
