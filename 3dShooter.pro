TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

unix|win32: LIBS += -lOpenGL
unix|win32: LIBS += -lGLEW
unix|win32: LIBS += -lOpenCL
unix|win32: LIBS += -ldl
unix|win32: LIBS += -lSDL2
unix|win32: LIBS += -lSDL2main
unix|win32: LIBS += -lSDL2_ttf
unix|win32: LIBS += -lassimp
unix|win32: LIBS += -lboost_filesystem
unix|win32: LIBS += -lboost_system

SOURCES += \
        camera.cpp \
        game.cpp \
        main.cpp \
        mesh.cpp \
        model3d.cpp \
        model3dcustom.cpp \
        object3d.cpp \
        objloader.cpp \
        plyloader.cpp \
        renderer.cpp \
        resourcemanager.cpp \
        shader.cpp \
        stb_image.cpp \
        world.cpp

HEADERS += \
    camera.h \
    game.h \
    mesh.h \
    model3d.h \
    model3dcustom.h \
    object3d.h \
    objloader.h \
    plyloader.h \
    renderer.h \
    resourcemanager.h \
    shader.h \
    stb_image.h \
    world.h

DISTFILES += \
    resources/models3d/9yqm6nw2el1c-drum_crate.rar \
    resources/models3d/drum_crate/barrel_and_crate.jpg \
    resources/models3d/n58d13f2bhmo-Crates.zip \
    resources/models3d/n58d13f2bhmo-Crates/Crates.fbx \
    resources/models3d/n58d13f2bhmo-Crates/Crates.mtl \
    resources/models3d/n58d13f2bhmo-Crates/textures/Crates.png \
    resources/models3d/n58d13f2bhmo-Crates/textures/Crates_Bump.png \
    resources/models3d/n58d13f2bhmo-Crates/textures/Crates_normal.png \
    resources/models3d/n58d13f2bhmo-Crates/textures/paper.png \
    resources/shaders/fragmentShader (copy).glsl \
    resources/shaders/fragmentShader.glsl \
    resources/shaders/vertexShader (copy).glsl \
    resources/shaders/vertexShader.glsl \
    resources/textures/HD_SEAL.jpg \
    resources/textures/Pebbled-Ground-3.jpg \
    resources/textures/awesomeface.png \
    resources/textures/brick-road-1-texture.jpg \
    resources/textures/container.jpg \
    resources/textures/flower1200.jpg \
    resources/textures/hqdefault.jpg \
    resources/textures/hqdefault90.jpg \
    resources/textures/jpgtest.jpg \
    resources/textures/linux.png \
    resources/textures/linuxLarge.png \
    resources/textures/tux.png

#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../usr/lib/x86_64-linux-gnu/release/ -lassimp
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../usr/lib/x86_64-linux-gnu/debug/ -lassimp
#else:unix: LIBS += -L$$PWD/../../../../../usr/lib/x86_64-linux-gnu/ -lassimp

#INCLUDEPATH += $$PWD/../../../../../usr/include/assimp
#DEPENDPATH += $$PWD/../../../../../usr/include/assimp

