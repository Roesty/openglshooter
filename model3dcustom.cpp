#include "model3dcustom.h"

Model3dCUSTOM::Model3dCUSTOM()
{

}

void Model3dCUSTOM::giveVertex(SimpleVertex newVertex)
{
    m_vertices.push_back(newVertex);
}

void Model3dCUSTOM::store()
{
    std::cout<<"Storing model"<<std::endl;
    std::cout<<"vertices size: "<<m_vertices.size()<<std::endl;

    // create buffers/arrays
    glGenVertexArrays(1, &m_VAO);
    glGenBuffers(1, &m_VBO);
    glGenBuffers(1, &m_EBO);

    glBindVertexArray(m_VAO);
    // load data into vertex buffers
    glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
    // A great thing about structs is that their memory layout is sequential for all its items.
    // The effect is that we can simply pass a pointer to the struct and it translates perfectly to a glm::vec3/2 array which
    // again translates to 3/2 floats which translates to a byte array.
    glBufferData(GL_ARRAY_BUFFER, m_vertices.size() * sizeof(SimpleVertex), &m_vertices[0], GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_indices.size() * sizeof(unsigned int), &m_indices[0], GL_STATIC_DRAW);

    // set the vertex attribute pointers
    // vertex positions
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(SimpleVertex), (void*)0);

    // vertex color
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(SimpleVertex), (void*)offsetof(SimpleVertex, color));

    // vertex normal
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(SimpleVertex), (void*)offsetof(SimpleVertex, normal));

    // vertex texture coord
    glEnableVertexAttribArray(3);
    glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, sizeof(SimpleVertex), (void*)offsetof(SimpleVertex, texCoords));

    glBindVertexArray(0);
}

GLuint Model3dCUSTOM::VAO() const
{
    return m_VAO;
}

GLuint Model3dCUSTOM::VBO() const
{
    return m_VBO;
}

GLuint Model3dCUSTOM::EBO() const
{
    return m_EBO;
}

GLuint Model3dCUSTOM::verticesSize() const
{
    return m_vertices.size();
}
