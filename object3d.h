#ifndef OBJECT3D_H
#define OBJECT3D_H

#define GLM_ENABLE_EXPERIMENTAL
#define GLOBAL_UP glm::vec3(0.0f, 1.0f, 0.0f)

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/ext.hpp>
#include <memory>

#include "model3d.h"
#include "resourcemanager.h"

enum eScope {
    SCOPE_LOCAL=0,
    SCOPE_GLOBAL
};

enum eEntity {
    ENTITY_OBJECT=0,
    ENTITY_GROUND,
    ENTITY_CAMERA
};

class Object3d
{
public:

    Object3d(std::string name="NO NAME");
    Object3d(const Object3d& other);
    ~Object3d();

    void translate(glm::vec3 vector);
    void rotate(glm::vec3 vector);
    void scale(glm::vec3 vector);

    void setToDefault();

    eEntity getType() const;
    void setType(eEntity type);

    glm::vec4 getPosition(eScope scope=SCOPE_LOCAL);
    glm::vec4 getRotation(eScope scope=SCOPE_LOCAL);
    glm::vec4 getScale(eScope scope=SCOPE_LOCAL);

    void setPosition(glm::vec3 position);
    void setRotation(glm::vec3 rotation);
    void setScale(glm::vec3 scale);

    bool hasModel();

    std::string getName() const;
    void setName(const std::string &name);

    std::shared_ptr<Object3d> getParent() const;
    void setParent( std::shared_ptr<Object3d> parent);

    Model3d getModel() const;
    Model3d* getModelPtr() const;
    void setModel(eModels model);
    void setModel(std::shared_ptr<Model3dCUSTOM> model);

    glm::mat4 getTrans() const;
    void setTrans(const glm::mat4 &trans);

    std::shared_ptr<Model3dCUSTOM> getModelCUSTOM() const;

    glm::vec3 getForward() const;
    glm::vec3 getRight() const;
    glm::vec3 getUp() const;

protected:
    std::shared_ptr<Object3d> m_parent;

    std::unique_ptr<Model3d> m_model;
    std::shared_ptr<Model3dCUSTOM> m_modelCUSTOM;

    eEntity m_type;

    glm::vec4 m_position;
    glm::vec4 m_rotation;
    glm::vec4 m_scale;

    //The directions for the object
    glm::vec3 m_forward;
    glm::vec3 m_right;
    glm::vec3 m_up;

    glm::mat4 m_trans;

private:
    std::string m_name;

    void determineDirections();

};

#endif // OBJECT3D_H
