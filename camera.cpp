#include "camera.h"

Camera::Camera()
{
    m_curProjection=PERSPECTIVE;

    m_object.reset();

    m_fov = 90.0f;
    m_w = 0.0f;
    m_h = 0.0f;
    m_near = 0.1f;
    m_far = 5000.0f;

    m_fovMin=0.01f;
    m_fovMax=120.0f;

    m_moveSpeed=25.0f;
    m_mouseSensitivity=0.5f;

    //yaw is initialized to -90.0 degrees since a yaw
    //of 0.0 results in a direction vector pointing to
    //the right so we initially rotate a bit to the left
    m_yaw = -90.0f;
    m_pitch = 0.0f;
}

Camera::eProjection Camera::getProjectionType()
{
    return m_curProjection;
}

glm::mat4 Camera::getProjection()
{
    switch (m_curProjection) {
    case PERSPECTIVE:
    {
        return glm::perspective(glm::radians(m_fov), m_w/m_h, m_near, m_far);
    }
    case ORTHOGRAPHIC:
    {
        SDL_Log("Ortho not implemented. Perspective was returned from this call.");
        m_curProjection = PERSPECTIVE;
        return getProjection();
    }
    }

    return m_projection;
}

glm::mat4 Camera::getView()
{
    return glm::lookAt(glm::vec3(getPosition()), glm::vec3(getPosition())+glm::vec3(getDirection()), up());
}

void Camera::setOrthographic(glm::mat4 orthographic)
{
    m_orthographic=orthographic;
}

void Camera::setObject(std::shared_ptr<Object3d> object)
{
    m_object=object;
//    m_object->setRotation(glm::normalize(glm::vec3(getPosition())-m_target));
}

glm::vec4 Camera::getPosition()
{
    return m_object->getPosition();
}

glm::vec4 Camera::getDirection()
{
    return m_object->getRotation();
}

glm::vec4 Camera::getScale()
{
    return m_object->getScale();
}

void Camera::translate(glm::vec3 translation)
{
    m_object->translate(translation);
}

void Camera::recalculatePerspective()
{
    SDL_Log("set perspective");
    m_perspective=glm::perspective(glm::radians(m_fov), m_w/m_h, m_near, m_far);
}

void Camera::resetPerspective(float fov, float window_w, float window_h, float near, float far)
{
    m_fov = fov;
    m_w = window_w;
    m_h = window_h;
    m_near = near;
    m_far = far;
}

void Camera::updateWindowSize(GLuint window_w, GLuint window_h)
{
    m_w = window_w;
    m_h = window_h;
}

void Camera::moveInDirection(Camera::eDirection direction, float scalar)
{
    float distance = m_moveSpeed*scalar;

    switch (direction) {
    case FORWARD:
    {
        translate(getDirection()*distance);
        break;
    }
    case BACKWARD:
    {
        translate(-getDirection()*distance);
        break;
    }
    case RIGHT:
    {
        translate(-right()*distance);
        break;
    }
    case LEFT:
    {
        translate(right()*distance);
        break;
    }
    case UP:
    {
        translate(up()*distance);
        break;
    }
    case DOWN:
    {
        translate(-up()*distance);
        break;
    }
    }
}

void Camera::mouseMotion(GLint relX, GLint relY)
{
    m_yaw += relX*m_mouseSensitivity*(m_fov/m_fovMax);
    m_pitch += relY*m_mouseSensitivity*(m_fov/m_fovMax);

    // make sure that when pitch is out of bounds, screen doesn't get flipped
    if (m_pitch > 89.9f)
        m_pitch = 89.9f;
    if (m_pitch < -89.9f)
        m_pitch = -89.9f;

    glm::vec3 front;
    front.x = cos(glm::radians(m_yaw)) * cos(glm::radians(m_pitch));
    front.y = sin(glm::radians(m_pitch));
    front.z = sin(glm::radians(m_yaw)) * cos(glm::radians(m_pitch));

    setDirection(glm::normalize(front));
}


void Camera::mouseScroll(GLint x, GLint y)
{
    m_fov -= static_cast<float>(y);

    if (m_fov<m_fovMin)
        m_fov=m_fovMin;

    if (m_fov>m_fovMax)
        m_fov=m_fovMax;
}

void Camera::setDirection(glm::vec3 direction)
{
    m_object->setRotation(direction);
}

glm::vec3 Camera::forward()
{
    return m_object->getForward();
}

glm::vec3 Camera::right()
{
    return m_object->getRight();
}

glm::vec3 Camera::up()
{
    return m_object->getUp();
}

void Camera::setToDefault()
{
}


































