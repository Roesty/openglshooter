#include "renderer.h"

Renderer::Renderer()
{
    m_w=1000;
    m_h=1000;

    m_renderDebug=true;

    // Create the window
    Uint32 flags = SDL_WINDOW_OPENGL|SDL_WINDOW_RESIZABLE;
    m_window.reset(std::move(SDL_CreateWindow("3D Shooter", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, m_w, m_h, flags)),
                   SDL_DestroyWindow);

    SDL_SetRelativeMouseMode(SDL_TRUE);

    //Disable the cursor
    SDL_ShowCursor(SDL_DISABLE);
    //Possibly replace it with custom
    //https://wiki.libsdl.org/SDL_CreateCursor

    // Create our opengl context and attach it to our window
    m_mainContext = SDL_GL_CreateContext(m_window.get());

    //the renderer defines the opengl context and attaches it
    //follow it up with glew
    glewExperimental = GL_TRUE;
    glewInit();

    resize(m_w, m_h);

    //load the shaders
    m_shader.reset(new Shader((SHADER_DIRECTORY+"vertexShader.glsl").c_str(),
                              (SHADER_DIRECTORY+"fragmentShader.glsl").c_str()));

    //tell opengl to use these shaders
    m_shader->use();// don't forget to activate/use the shader before setting uniforms!

    m_programID=0;

    m_backgroundColor = glm::vec3(0.3f, 0.3f, 0.5f);
    //m_backgroundColor = glm::vec3(0.0f, 0.0f, 0.0f);

    m_model = glm::mat4(1.0f);

    m_wireframe=false;
//    flipWireframe();
}

Renderer::~Renderer()
{
    // Delete our OpengL context
    SDL_GL_DeleteContext(m_mainContext);

    // Destroy our window
    SDL_DestroyWindow(m_window.get());
}

//renders the entire world
void Renderer::render()
{
    glClearColor(m_backgroundColor.x, m_backgroundColor.y, m_backgroundColor.z, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    m_shader->use();

    m_shader->setMat4("projection", m_mainCamera.getProjection());
    m_shader->setMat4("view", m_mainCamera.getView());

    float seconds = (float)SDL_GetTicks()/1000.0f;

    std::vector<std::shared_ptr<Object3d>>* entities = m_world->getEntitiesReference();

    for (GLuint iii=0; iii<entities->size(); iii++)
    {
        Object3d* entity = (*entities)[iii].get();
        Model3d* model = entity->getModelPtr();

        //Skip the object if is doesnt have a model
        if (!entity->hasModel())
            continue;

        glm::mat4 modelMatrix = glm::mat4(1.0f);
        modelMatrix = glm::translate(modelMatrix, glm::vec3(entity->getPosition()));
        //        modelMatrix = glm::scale(modelMatrix, glm::vec3(0.1, 0.1, 0.1));
        //        modelMatrix = glm::scale(modelMatrix, glm::vec3(1.2, 1, 1));

        m_shader->setMat4("model", modelMatrix);

        //draw all meshes for this model
        for(GLuint i = 0; i < model->m_meshes.size(); i++)
        {
            // bind appropriate textures
            unsigned int diffuseNr  = 1;
            unsigned int specularNr = 1;
            unsigned int normalNr   = 1;
            unsigned int heightNr   = 1;
            for(unsigned int i = 0; i < model->m_meshes[i].textures.size(); i++)
            {
                glActiveTexture(GL_TEXTURE0 + i); // active proper texture unit before binding
                // retrieve texture number (the N in diffuse_textureN)
                std::string number;
                std::string name = model->m_meshes[i].textures[i].type;
                if(name == "texture_diffuse")
                    number = std::to_string(diffuseNr++);
                else if(name == "texture_specular")
                    number = std::to_string(specularNr++); // transfer unsigned int to stream
                else if(name == "texture_normal")
                    number = std::to_string(normalNr++); // transfer unsigned int to stream
                else if(name == "texture_height")
                    number = std::to_string(heightNr++); // transfer unsigned int to stream

                // now set the sampler to the correct texture unit
                glUniform1i(glGetUniformLocation(m_shader->ID, (name + number).c_str()), i);
                // and finally bind the texture
                glBindTexture(GL_TEXTURE_2D, model->m_meshes[i].textures[i].id);
            }

            // draw mesh
            glBindVertexArray(model->m_meshes[i].VAO);

            glDrawElements(GL_TRIANGLES, model->m_meshes[i].indices.size(), GL_UNSIGNED_INT, 0);
            glBindVertexArray(0);

            // always good practice to set everything back to defaults once configured.
            glActiveTexture(GL_TEXTURE0);
        }

    }
    SDL_GL_SwapWindow(m_window.get());

    return;
    /*
    //fill the screen with the background color
    glClearColor(m_backgroundColor.x, m_backgroundColor.y, m_backgroundColor.z, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    // render the world
    std::vector<std::shared_ptr<Object3d>>* entities = m_world->getEntitiesReference();

    float seconds = (float)SDL_GetTicks()/1000.0f;

    m_shader->use();
    m_shader->setMat4("projection", m_mainCamera.getProjection());

    for (unsigned int iii=0; iii<entities->size(); iii++)
    {
        Object3d* entity = (*entities)[iii].get();
        Model3d* model = entity->getModelPtr();

        //Skip the object if is doesnt have a model
        if (!entity->hasModel())
            continue;

        for (unsigned int iii = 0; iii < model->getTextures().size(); iii++)
        {
            glActiveTexture(iii);
            glBindTexture(GL_TEXTURE_2D, model->getTextures()[iii].id);

            switch (iii)
            {
            case 0:
            {
                glActiveTexture(GL_TEXTURE0);
                glBindTexture(GL_TEXTURE_2D, model->getTextures()[iii].id);
                break;
            }
            case 1:
            {
                glActiveTexture(GL_TEXTURE1);
                glBindTexture(GL_TEXTURE_2D, model->getTextures()[iii].id);
                break;
            }

            }
        }

        glm::mat4 view = glm::lookAt(
                    glm::vec3(m_mainCamera.getPosition()),
                    glm::vec3(m_mainCamera.getPosition())+glm::vec3(m_mainCamera.getDirection()),
                    m_mainCamera.getUp());
        m_shader->setMat4("view", view);

        glBindVertexArray(model->VAO());

        // calculate the model matrix for each object and pass it to shader before drawing
        glm::mat4 modelMatrix = glm::mat4(1.0f); // make sure to initialize matrix to identity matrix first
        modelMatrix = glm::translate(modelMatrix, glm::vec3(entity->getPosition()));

        float angle = 20.0f * (iii) * seconds;
        modelMatrix = glm::rotate(modelMatrix, glm::radians(angle), glm::vec3(1.0f, 0.3f, 0.5f));
        m_shader->setMat4("model", modelMatrix);

        glDrawArrays(GL_TRIANGLES, 0, 36);
    }

            */
}

void Renderer::render2()
{
    glClearColor(m_backgroundColor.x, m_backgroundColor.y, m_backgroundColor.z, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    m_shader->use();

    m_shader->setMat4("projection", m_mainCamera.getProjection());
    m_shader->setMat4("view", m_mainCamera.getView());

    float seconds = (float)SDL_GetTicks()/1000.0f;

    std::vector<std::shared_ptr<Object3d>>* entities = m_world->getEntitiesReference();

    for (GLuint iii=0; iii<entities->size(); iii++)
    {
        Object3d* entity = (*entities)[iii].get();
        std::shared_ptr<Model3dCUSTOM> model = entity->getModelCUSTOM();

        //Skip the object if is doesnt have a model
        if (!model.get())
        {
            if (entity->getType()!=ENTITY_CAMERA)
            {
                if (m_renderDebug)
                {
                    SDL_Log("Skipped rendering of %s", entity->getName().c_str());
                    m_renderDebug=false;
                }
            }
            continue;
        }

        glLineWidth(5.0f);

        m_model = glm::mat4(1.0f);

        /// ANIMATION
//        m_model = glm::rotate(m_model, seconds*0.0001f*iii, glm::vec3(0.0, cos(seconds), sin(seconds)));

        m_model = glm::translate(m_model, glm::vec3(entity->getPosition(SCOPE_GLOBAL)));
        m_model = glm::scale(m_model, glm::vec3(entity->getScale()));

//        m_model = glm::rotate(m_model, 1.0f, glm::vec3(entity->getRotation()));

//                m_model = glm::scale(m_model, glm::vec3(0.1, 0.1, 0.1));
//                m_model = glm::scale(m_model, glm::vec3(3, 3, 3));
//        m_model = glm::scale(m_model, glm::vec3(5, 0.5f, 5));

        m_shader->setMat4("model", m_model);

        // draw mesh
        glBindVertexArray(model->VAO());
        glDrawArrays(GL_TRIANGLES, 0, model->verticesSize());
        glBindVertexArray(0);
    }

    SDL_GL_SwapWindow(m_window.get());
}

void Renderer::updateViewBox()
{
    glViewport(0, 0, static_cast<int>(m_w), static_cast<int>(m_h));
    m_mainCamera.updateWindowSize(m_w, m_h);
}

void Renderer::resize(GLuint w, GLuint h)
{
    SDL_Log("Window resized to %dx%d", w, h);
    m_w = w;
    m_h = h;

    updateViewBox();
}

void Renderer::setWorld(std::shared_ptr<World>* world)
{
    //Assign the pointer to the shared pointer
    m_world = *world;
}

void Renderer::useFirstCamera()
{
    m_mainCamera.setObject(m_world->getFirstCamera());
}

void Renderer::loadTextures()
{
    /*
    GLuint textureID;

    std::vector<std::shared_ptr<Object3d>>* entities = m_world->getEntitiesReference();

    for (unsigned int iii=0; iii<entities->size(); iii++)
    {
        Object3d* entity = (*entities)[iii].get();
        Model3d* model = entity->getModelPtr();

        if (model==nullptr)
        {
            continue;
        }

        std::vector<Texture> modelTextures=model->getTextures();

        for (unsigned int jjj=0; jjj<modelTextures.size(); jjj++)
        {
            //check if this model's desired texture is in the loaded textures

            bool textureLoaded = false;
            for (unsigned int kkk = 0; kkk < m_textures.size(); kkk++)
            {
                if (modelTextures[jjj].path==m_textures[kkk].path)
                {
                    model->assignTexture(m_textures[kkk]);
                    textureLoaded = true;
                    std::cout<<"Assigned "<<modelTextures[jjj].path<<std::endl;
                    break;
                }
            }

            if (!textureLoaded)
            {
                // load image, create texture and generate mipmaps
                GLint width, height, nrChannels;
                stbi_set_flip_vertically_on_load(true); // tell stb_image.h to flip loaded texture's on the y-axis.
                std::string texturePath = TEXTURE_DIRECTORY+modelTextures[jjj].path;

                unsigned char *data = stbi_load(texturePath.c_str(), &width, &height, &nrChannels, 0);
                if (data)
                {
                    glGenTextures(1, &textureID);
                    glBindTexture(GL_TEXTURE_2D, textureID);

                    // set the texture wrapping parameters
                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

                    // set texture filtering parameters
                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

                    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
                    std::cout<<"Loaded "<<modelTextures[jjj].path<<std::endl;
                    glGenerateMipmap(GL_TEXTURE_2D);

                    modelTextures[jjj].type="";
                    modelTextures[jjj].id=textureID;

                    m_textures.push_back(modelTextures[jjj]);
                    model->assignTexture(modelTextures[jjj]);

                    stbi_image_free(data);
                }
                else
                {
                    std::cout << "Failed to load texture: '"<<modelTextures[jjj].path<<"'" << std::endl;
                }
            }
        }
    }
    */
}

void Renderer::moveCamera(Camera::eDirection direction, float scalar)
{
    m_mainCamera.moveInDirection(direction, scalar);
}

void Renderer::mouseMotion(GLint relX, GLint relY)
{
    m_mainCamera.mouseMotion(relX, relY);
}

void Renderer::mouseScroll(GLint x, GLint y)
{
    m_mainCamera.mouseScroll(x, y);
}

void Renderer::flipWireframe()
{
    m_wireframe=!m_wireframe;

    if (m_wireframe)
    {
        glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
    }
    else
    {
        glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
    }
}

glm::vec3 Renderer::getCameraPosition()
{
    return glm::vec3(m_mainCamera.getPosition());
}








































