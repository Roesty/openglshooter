#include "objloader.h"

using namespace std;

ObjLoader::ObjLoader()
{
    m_error.clear();
    m_colorDetected=false;
}

std::shared_ptr<Model3d> ObjLoader::loadFile(std::string fileName)
{
    string line;

    ifstream objFile(fileName);

    std::shared_ptr<Model3d> loadedModel(nullptr);

    int n=0;

    try {
        if (objFile.is_open())
        {

            while (getline(objFile, line))
            {
                n++;

                while (true)
                {
                    //locate double space
                    int index = line.find("  ", 0);

                    if (index == std::string::npos)
                    {
                        break;
                    }

                    // replace it
                    line.replace(index, 2, " ");
                }

                if (line=="")
                {
                    continue;
                }

                //With the string cleared, start extracting the tokens
                vector<string> tokens = splitString(line);

                /*
            //Iterate all the tokens to see what is there
            for (unsigned int iii=0; iii<tokens.size(); iii++)
            {
                cout<<tokens[iii]<<"|";
            }
            cout<<endl;
*/

                //Is the line a group?
                if (tokens[0]=="g")
                {
                    continue;
                }

                //Is the line a vertex?
                if (tokens[0]=="v")
                {
                    m_vertices.push_back(lVertex());

                    m_vertices.back().x=stof(tokens[1].c_str());
                    m_vertices.back().y=stof(tokens[2].c_str());
                    m_vertices.back().z=stof(tokens[3].c_str());

                    if (tokens.size() > 4)
                    {
                        m_vertices.back().r=stof(tokens[4].c_str());
                        m_vertices.back().g=stof(tokens[5].c_str());
                        m_vertices.back().b=stof(tokens[6].c_str());
                        m_colorDetected=true;
                    }
                    else
                    {
                        m_vertices.back().r=-1;
                        m_vertices.back().g=-1;
                        m_vertices.back().b=-1;
                    }

                    continue;
                }

                //Is the line a vertex texture coord?
                if (tokens[0]=="vt")
                {
                    m_textureCoords.push_back(lTextureCoord());

                    m_textureCoords.back().u=stof(tokens[1].c_str());

                    if (tokens.size() > 2)
                    {
                        m_textureCoords.back().v=stof(tokens[2].c_str());
                        if (tokens.size() > 3)
                        {
                            m_textureCoords.back().w=stof(tokens[3].c_str());
                        }
                        else
                        {
                            m_textureCoords.back().w=0;
                        }
                        m_textureCoords.back().v=0;
                        m_textureCoords.back().w=0;
                    }

                    continue;
                }

                //Is the line a vertex normal?
                if (tokens[0]=="vn")
                {
                    m_vertexNormals.push_back(lVertexNormal());

                    m_vertexNormals.back().x=stof(tokens[1].c_str());
                    m_vertexNormals.back().y=stof(tokens[2].c_str());
                    m_vertexNormals.back().z=stof(tokens[3].c_str());

                    continue;
                }

                //Is the line a face?
                if (tokens[0]=="f")
                {
                    lFace newFace;

                    for (unsigned int iii=1; iii<tokens.size(); iii++)
                    {
                        vector<string> data = splitString(tokens[iii], "/");

                        switch (data.size())
                        {
                        case 1:
                        {
                            newFace.vertices.push_back(stoi(data[0]));
                            break;
                        }
                        case 3:
                        {
                            newFace.vertices.push_back(stoi(data[0]));
                            if (data[1]!="")
                            {
                                newFace.vertexNormals.push_back(stoi(data[1]));
                            }
                            newFace.vertices.push_back(stoi(data[2]));
                            break;
                        }
                        case 4:
                        {
                            break;
                        }
                        case 5:
                        {
                            break;
                        }

                        }
                    }

                    m_faces.push_back(newFace);
                    continue;
                }

                SDL_Log("Did not handle line '%s'", line.c_str());
            }
            objFile.close();
        }
        else
        {
            m_error="Could not open file '"+fileName+"'.";
            return loadedModel;
        }

    }
    catch (exception e)
    {
        m_error = "Error at line"+to_string(n);
        return loadedModel;
    }

    cout<<"Success!"<<endl;
    cout<<"'"<<splitString(fileName, "/").back()<<"' consists of:"<<endl;
    cout<<"           Faces: "<<m_faces.size()<<endl;
    cout<<"        Vertices: "<<m_vertices.size()<<endl;
    cout<<"  Texture Coords: "<<m_textureCoords.size()<<endl;
    cout<<"  Vertex Normals: "<<m_vertexNormals.size()<<endl;

    //use the data to create the VAO, VBO and EBO
    for (GLuint faceIndex=0; faceIndex<m_faces.size(); faceIndex++)
    {
        //Start at two 2 store the vertices from the current n and n-1 and n-2
        for (GLuint n=2; n<m_faces[faceIndex].vertices.size(); n++)
        {
            vector<SimpleVertex> triangle;

            for (GLuint iii=0; iii<3; iii++)
            {
                SimpleVertex vertex;
                //Insert a new vertex
                triangle.push_back(SimpleVertex());

                //Set the position
                vertex.position=glm::vec3(
                            m_vertices[m_faces[faceIndex].vertices[n-iii]].x,
                        m_vertices[m_faces[faceIndex].vertices[n-iii]].y,
                        m_vertices[m_faces[faceIndex].vertices[n-iii]].z
                        );

                //Set the color
                vertex.color=glm::vec3(
                            m_vertices[m_faces[faceIndex].vertices[n-iii]].r,
                        m_vertices[m_faces[faceIndex].vertices[n-iii]].g,
                        m_vertices[m_faces[faceIndex].vertices[n-iii]].b
                        );

                //Set the texture coordinates
                vertex.color=glm::vec3(
                            m_textureCoords[m_faces[faceIndex].textureCoords[n-iii]].u,
                        m_textureCoords[m_faces[faceIndex].textureCoords[n-iii]].v,
                        m_textureCoords[m_faces[faceIndex].textureCoords[n-iii]].w
                        );

                //Set the normals
                vertex.color=glm::vec3(
                            m_vertexNormals[m_faces[faceIndex].vertexNormals[n-iii]].x,
                        m_vertexNormals[m_faces[faceIndex].vertexNormals[n-iii]].y,
                        m_vertexNormals[m_faces[faceIndex].vertexNormals[n-iii]].z
                        );

                loadedModel->giveVertex(vertex);
            }
        }
    }

    return loadedModel;
}

std::shared_ptr<Model3dCUSTOM> ObjLoader::loadFileCUSTOM(string fileName)
{
    ifstream objFile(fileName);

    std::shared_ptr<Model3dCUSTOM> loadedModel(new Model3dCUSTOM());

    int n=0;

    SDL_Log("Object loader is running for:\n  %s", fileName.c_str());
    string line;
    try {
        if (objFile.is_open())
        {

            while (getline(objFile, line))
            {
                n++;

                while (true)
                {
                    //locate double space
                    int index = line.find("  ", 0);

                    if (index == std::string::npos)
                    {
                        break;
                    }

                    // replace it
                    line.replace(index, 2, " ");
                }

                if (line=="")
                {
                    continue;
                }

                //With the string cleared, start extracting the tokens
                vector<string> tokens = splitString(line);

                //Is the line a comment?
                if (tokens[0]=="#")
                {
                    continue;
                }

                //Is the line a group?
                if (tokens[0]=="g")
                {
                    continue;
                }

                //Is the line a vertex?
                if (tokens[0]=="v")
                {
                    m_vertices.push_back(lVertex());

                    m_vertices.back().x=stof(tokens[1].c_str());
                    m_vertices.back().y=stof(tokens[2].c_str());
                    m_vertices.back().z=stof(tokens[3].c_str());

                    if (tokens.size() > 4)
                    {
                        m_vertices.back().r=stof(tokens[4].c_str());
                        m_vertices.back().g=stof(tokens[5].c_str());
                        m_vertices.back().b=stof(tokens[6].c_str());
                        m_colorDetected=true;
                    }
                    else
                    {
                        m_vertices.back().r=-1;
                        m_vertices.back().g=-1;
                        m_vertices.back().b=-1;
                    }

                    continue;
                }

                //Is the line a vertex texture coord?
                if (tokens[0]=="vt")
                {
                    m_textureCoords.push_back(lTextureCoord());

                    m_textureCoords.back().u=stof(tokens[1].c_str());

                    if (tokens.size() > 2)
                    {
                        m_textureCoords.back().v=stof(tokens[2].c_str());
                        if (tokens.size() > 3)
                        {
                            m_textureCoords.back().w=stof(tokens[3].c_str());
                        }
                        else
                        {
                            m_textureCoords.back().w=0;
                        }
                    }
                    else
                    {
                        m_textureCoords.back().v=0;
                        m_textureCoords.back().w=0;
                    }

                    continue;
                }

                //Is the line a vertex normal?
                if (tokens[0]=="vn")
                {
                    m_vertexNormals.push_back(lVertexNormal());

                    m_vertexNormals.back().x=stof(tokens[1].c_str());
                    m_vertexNormals.back().y=stof(tokens[2].c_str());
                    m_vertexNormals.back().z=stof(tokens[3].c_str());

                    continue;
                }

                //Is the line a face?
                if (tokens[0]=="f")
                {
                    lFace newFace;

                    for (unsigned int iii=1; iii<tokens.size(); iii++)
                    {
                        vector<string> data = splitString(tokens[iii], "/");

                        switch (data.size())
                        {
                        case 1:
                        {
                            newFace.vertices.push_back(stoi(data[0]));
                            newFace.textureCoords.push_back(0);
                            newFace.vertexNormals.push_back(0);
                            break;
                        }
                        case 3:
                        {
                            newFace.vertices.push_back(stoi(data[0]));
                            if (data[1]!="")
                            {
                                newFace.textureCoords.push_back(stoi(data[1]));
                            }
                            else
                            {
                                newFace.textureCoords.push_back(0);
                            }
                            newFace.vertexNormals.push_back(stoi(data[2]));
                            break;
                        }
                        case 4:
                        {
                            break;
                        }
                        case 5:
                        {
                            break;
                        }

                        }
                    }

                    m_faces.push_back(newFace);
                    continue;
                }

                SDL_Log("Did not handle line '%s', unkown token.", line.c_str());
            }
            objFile.close();
        }
        else
        {
            m_error="Could not open file '"+fileName+"'.";
            return nullptr;
        }

    }
    catch (exception e)
    {
        m_error = "Error at line"+to_string(n);
        return nullptr;
    }


    cout<<"Success!"<<endl;
    cout<<"'"<<splitString(fileName, "/").back()<<"' consists of:"<<endl;
    cout<<"           Faces: "<<m_faces.size()<<endl;
    cout<<"        Vertices: "<<m_vertices.size()<<endl;
    cout<<"  Texture Coords: "<<m_textureCoords.size()<<endl;
    cout<<"  Vertex Normals: "<<m_vertexNormals.size()<<endl;

    //use the data to create the VAO, VBO and EBO
    for (GLuint faceIndex=0; faceIndex<m_faces.size(); faceIndex++)
    {

        //Start at two 2 store the vertices from the current n and n-1 and n-2
        for (GLuint n=2; n<m_faces[faceIndex].vertices.size(); n++)
        {
            /*
            cout<<m_faces[faceIndex].textureCoords[n]<<endl;
            cout<<m_faces[faceIndex].textureCoords[n]<<endl;
            cout<<m_faces[faceIndex].textureCoords[n]<<endl;
*/
            vector<SimpleVertex> triangle;

            for (GLuint iii=0; iii<3; iii++)
            {
                /*
                cout<<"Face: "<<faceIndex<<endl;
                cout<<"Vertex: "<<n<<" (always starts at 2)"<<endl;
                cout<<"Triangle: "<<iii<<endl;
*/
                //Insert a new vertex
                SimpleVertex vertex;
                triangle.push_back(SimpleVertex());

                GLuint vertexIndex = m_faces[faceIndex].vertices[n-iii];
                GLuint textureIndex = m_faces[faceIndex].textureCoords[n-iii];
                GLuint normalIndex = m_faces[faceIndex].vertexNormals[n-iii];

                //Set the position
                vertex.position=glm::vec3(m_vertices[vertexIndex-1].x,
                                          m_vertices[vertexIndex-1].y,
                                          m_vertices[vertexIndex-1].z);

                //Set the color
                vertex.color=glm::vec3(m_vertices[vertexIndex-1].r,
                                       m_vertices[vertexIndex-1].g,
                                       m_vertices[vertexIndex-1].b);

                //If there are no texture coords for this object, then this value will be 0
                if (textureIndex)
                {
                    //Set the texture coordinates to what was read
                    vertex.texCoords=glm::vec3(m_textureCoords[textureIndex-1].u,
                                               m_textureCoords[textureIndex-1].v,
                                               m_textureCoords[textureIndex-1].w);
                }
                else
                {
                    //Set the texture coordinates to basic nulls
                    vertex.texCoords=glm::vec3(0.0f, 0.0f, 0.0f);
                }

                //Set the normals
                if (normalIndex)
                {
                    vertex.normal=glm::vec3(m_vertexNormals[normalIndex-1].x,
                                            m_vertexNormals[normalIndex-1].y,
                                            m_vertexNormals[normalIndex-1].z);
                }
                else
                {
                    vertex.normal=glm::vec3(0.0f, 0.0f, 0.0f);
                }

                loadedModel->giveVertex(vertex);
            }
        }
    }

    //Store the model before returning the reference
    //After storing the model, it is ready to use
    loadedModel->store();

    m_vertices.clear();
    m_textureCoords.clear();
    m_vertexNormals.clear();
    m_faces.clear();

    return loadedModel;
}

std::string ObjLoader::getError()
{
    return m_error;
}

std::vector<string> ObjLoader::splitString(string inputString, string delimiter)
{
    vector<string> tokens;
    string token;
    int previousIndex=0;

    int index=0;
    while (true)
    {
        index = inputString.find(delimiter, index);
        //cout<<previousIndex<<" "<<index<<endl;

        //check if the end of the string has been reached
        if (index == std::string::npos)
        {
            tokens.push_back(inputString.substr(previousIndex));
            return tokens;
        }

        tokens.push_back(inputString.substr(previousIndex, index-previousIndex));

        previousIndex=index+1;
        index = previousIndex;
    }
}
































































