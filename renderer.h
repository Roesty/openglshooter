#ifndef RENDERER_H
#define RENDERER_H

#include <GL/glew.h>
#include <GL/gl.h>
#include <SDL2/SDL.h>
#include <memory>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "shader.h"
#include "world.h"
#include "model3d.h"
#include "resourcemanager.h"

class Renderer
{
public:
    Renderer();
    ~Renderer();

    void resizeWindow(GLuint w, GLuint h);
    void render();
    void render2();
    void updateViewBox();
    void resize(GLuint w, GLuint h);
    void setWorld(std::shared_ptr<World>* world);
    void useFirstCamera();

    void loadTextures();

    void moveCamera(Camera::eDirection direction, float scalar);
    void mouseMotion(GLint relX, GLint relY);
    void mouseScroll(GLint x, GLint y);

    void flipWireframe();

    glm::vec3 getCameraPosition();

private:
    const std::string RESOURCE_DIRECTORY="/home/lace/QtProjects/3dShooter/3dShooter/resources/";
    const std::string SHADER_DIRECTORY=RESOURCE_DIRECTORY+"shaders/";

    ///Window stuff
    //the main window of the application
    //is supposed to be unique_ptr, but I could not get it working
    std::shared_ptr<SDL_Window> m_window;

    //our opengl context handle
    SDL_GLContext m_mainContext;

    //window width and height
    GLuint m_w, m_h;

    ///Rendering stuff
    //World object that is to be rendered
    std::shared_ptr<World> m_world;

    //shader loader
    std::unique_ptr<Shader> m_shader;

    //the shader program id
    GLuint m_programID;

    //color for the background
    glm::vec3 m_backgroundColor;

    //loaded textures. these IDs are sent to the models to render them
    std::vector<Texture> m_textures;

    bool m_wireframe;

    ///World manipulation stuff
    glm::mat4 m_model;

    //// THIS IS THE CAMERA
    Camera m_mainCamera;

    bool m_renderDebug;
};

#endif // RENDERER_H
