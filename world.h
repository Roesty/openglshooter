#ifndef WORLD_H
#define WORLD_H

#include "object3d.h"
#include "camera.h"
#include <glm/ext.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <vector>

class World
{
public:
    World();
    ~World();

    void testWorld();
    void testWorld2();

    void insertObject(Object3d* newObject);

    void printContents();

    std::vector<std::shared_ptr<Object3d> >* getEntitiesReference();

    //Returns a pointer to the first object in the world that is of type ENTITY_CAMERA
    std::shared_ptr<Object3d> getFirstCamera();

    void generateTemplates();

private:
    //All the objects in the world
    //It contains shared pointers to Object3D
    //This is only the transform of objects in world space, derived objects share this transform
    std::vector<std::shared_ptr<Object3d>> m_entities;
    std::shared_ptr<Object3d> last();

    void insertObject(std::string name);

    //Holds objects that have VAOs, VBOs and EBOs
    //these are copied into the entities if wanted
    std::vector<Object3d> m_templates;

    ResourceManager m_resourceManager;

};

#endif // WORLD_H
