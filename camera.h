#ifndef CAMERA_H
#define CAMERA_H

#include "object3d.h"

#include <GL/gl.h>
#include <SDL2/SDL.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

class Camera
{
public:

    enum eProjection {
        PERSPECTIVE=0,
        ORTHOGRAPHIC
    };

    enum eDirection {
        FORWARD=0,
        BACKWARD,
        RIGHT,
        LEFT,
        UP,
        DOWN
    };

    Camera();

    void setProjectionType(eProjection projection);
    eProjection getProjectionType();

    glm::mat4 getProjection();
    glm::mat4 getView();

    void setPerspective(GLfloat fovy, GLfloat aspect, GLfloat zNear, GLfloat zFar);
    void setOrthographic(glm::mat4 orthographic);

    void setObject(std::shared_ptr<Object3d> position);

    glm::vec4 getPosition();
    glm::vec4 getDirection();
    glm::vec4 getScale();

    void setToDefault();

    void translate(glm::vec3 translation);

    void resetPerspective(float fov, float window_w, float window_h, float near, float far);
    void updateWindowSize(GLuint  window_w, GLuint window_h);

    glm::vec3 getUp() const;
    void setUp(const glm::vec3 &up);

    void moveInDirection(eDirection direction, float scalar=1.0f);

    void mouseMotion(GLint relX, GLint relY);

    void mouseScroll(GLint x, GLint y);

    void setDirection(glm::vec3 direction);

private:
    std::shared_ptr<Object3d> m_object;

    eProjection m_curProjection;
    glm::mat4 m_projection;

    // Projection matrix : 45° Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
    glm::mat4 m_perspective;
    //In world coordinates
    glm::mat4 m_orthographic;

    glm::vec3 forward();
    glm::vec3 right();
    glm::vec3 up();


    void recalculatePerspective();

    //projection stuff
    GLfloat m_fov;
    GLfloat m_w;
    GLfloat m_h;
    GLfloat m_near;
    GLfloat m_far;

    GLfloat m_fovMin;
    GLfloat m_fovMax;

    /*
    //rotation stuff
    glm::vec3 m_target;
    //this is the global up, it will always be (0.0, 1.0, 0.0)
    glm::vec3 m_globalUp;
    //right and up locally for the camera
    glm::vec3 m_right;
    glm::vec3 m_up;
*/

    float m_moveSpeed;
    float m_mouseSensitivity;

    float m_yaw;
    float m_pitch;

};

#endif // CAMERA_H
