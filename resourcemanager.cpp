#include "resourcemanager.h"

ResourceManager::ResourceManager()
{
}

void ResourceManager::start()
{
    vector<string> paths;
    paths.push_back("obj/generics/cube.obj");
    paths.push_back("obj/generics/pyramid.obj");
    paths.push_back("obj/crates/Crates.obj");
    paths.push_back("obj/grayBox/boxs.obj");
    paths.push_back("obj/bullet/shareablebullet.obj");
    paths.push_back("obj/dog/13466_Canaan_Dog_v1_L3.obj");
    paths.push_back("obj/bear/bear2.obj");
    paths.push_back("obj/pistolAmmoBox/box.obj");
    paths.push_back("obj/plainMan/FinalBaseMesh.obj");
    paths.push_back("obj/backpack/backpack.obj");
    paths.push_back("obj/45ACP/Handgun_obj.obj");
    paths.push_back("obj/house/house1.obj");
    paths.push_back("obj/house2/house_obj.obj");
    paths.push_back("obj/city/scifi tropical city/Sci-fi Tropical city.obj");
    paths.push_back("obj/gourd.obj");
    paths.push_back("obj/stingSword.obj");
    paths.push_back("obj/generics/diamond.obj");
    paths.push_back("obj/generics/dodecahedron.obj");
    paths.push_back("obj/generics/icosahedron.obj");
    paths.push_back("obj/generics/octahedron.obj");

    for (GLuint iii = 0; iii < MAX_MODELS; ++iii)
    {
        ModelReference newReference;
        newReference.path=paths[iii];
        newReference.reference=std::shared_ptr<Model3dCUSTOM>(nullptr);
        m_models.push_back(newReference);
    }
    std::cout<<"Loaded "<<m_models.size()<<" references\n";
}

void ResourceManager::stop()
{
}

void ResourceManager::clean()
{
}

std::shared_ptr<Model3dCUSTOM> ResourceManager::getModel(eModels model)
{
    switch (model)
    {
    case CUBE:            return getModel("obj/generics/cube.obj");
    case PYRAMID:         return getModel("obj/generics/pyramid.obj");
    case DIAMOND:         return getModel("obj/generics/diamond.obj");
    case DODECAHEDRON:    return getModel("obj/generics/dodecahedron.obj");
    case ICOSAHEDRON:     return getModel("obj/generics/icosahedron.obj");
    case OCTAHEDRON:      return getModel("obj/generics/octahedron.obj");
    case CRATE:           return getModel("obj/crates/Crates.obj");
    case GRAY_BOX:        return getModel("obj/grayBox/boxs.obj");
    case BULLET:          return getModel("obj/bullet/shareablebullet.obj");
    case DOG:             return getModel("obj/dog/13466_Canaan_Dog_v1_L3.obj");
    case BEAR:            return getModel("obj/bear/bear2.obj");
    case PISTOL_AMMO_BOX: return getModel("obj/pistolAmmoBox/box.obj");
    case PLAIN_MAN:       return getModel("obj/plainMan/FinalBaseMesh.obj");
    case BACKPACK:        return getModel("obj/backpack/backpack.obj");
    case HANDGUN_45ACP:   return getModel("obj/45ACP/Handgun_obj.obj");
    case HOUSE:           return getModel("obj/house/house1.obj");
    case HOUSE2:          return getModel("obj/house2/house_obj.obj");
    case CITY:            return getModel("obj/city/scifi tropical city/Sci-fi Tropical city.obj");
    case GOURD:           return getModel("obj/gourd.obj");
    case SWORD:           return getModel("obj/stingSword.obj");
    case MAX_MODELS:
    {
        return nullptr;
    }
    }
}

std::shared_ptr<Model3dCUSTOM> ResourceManager::getModel(string path)
{
    SDL_Log("Getting model");
    using namespace std;
    for (GLuint iii=0; iii<m_models.size(); iii++)
    {
        if (m_models[iii].path==path)
        {
            //Check if there is a use counter
            //This will be 0 if there is no object, meaning that the object is not loaded
            if (!m_models[iii].reference.get())
            {
                SDL_Log("Loading model");
                m_models[iii].reference=load(path);
            }
//            SDL_Log("%u", m_models[iii].reference.get());
            return m_models[iii].reference;
        }
    }

    return nullptr;
}

std::shared_ptr<Model3dCUSTOM> ResourceManager::load(string path)
{
    std::string fullPath = MODELS3D_DIRECTORY+path;

    std::shared_ptr<Model3dCUSTOM> loadedModel;

    if (fullPath.substr(fullPath.find_last_of("."))==".obj")
    {
        std::cout<<"loading object"<<std::endl;

        loadedModel=m_objLoader.loadFileCUSTOM(fullPath);
        if (!loadedModel.get())
        {
            std::cout<<".OBJ ERROR: "<<m_objLoader.getError()<<std::endl;
        }
        std::cout<<"loaded '"<<path<<"'"<<std::endl;
    }

    if (fullPath.substr(fullPath.find_last_of("."))==".ply")
    {
        std::cout<<"loading ply"<<std::endl;
        if (!m_plyLoader.loadFile(fullPath))
        {
            std::cout<<m_plyLoader.getError()<<std::endl;
        }
    }

    return loadedModel;
}

void ResourceManager::unload(string path)
{

}

void ResourceManager::createReferences(std::string path)
{
    /*
    namespace fs = boost::filesystem;

    fs::path targetDir(MODELS3D_DIRECTORY.append(path));

    fs::directory_iterator it(targetDir), eod;

    BOOST_FOREACH(fs::path const &p, std::make_pair(it, eod))
    {
        if(fs::is_regular_file(p))
        {
            if (p.extension()==".obj" || p.extension()==".ply")
            {
//                m_models.push_back(ModelReference());
//                m_models.back()->path=;
            }
            std::cout<<p<<std::endl;
        }

        if (fs::is_directory(p))
        {
            std::cout<<p<<std::endl;

        }
    }
    */
}





























































