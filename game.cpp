#include "game.h"

Game::Game()
{
    m_elapsedFrames=0;
    m_startTick=0;

    isRunning = false;

    camx=0.0f;
    camy=0.0f;
    camz=0.0f;

    m_renderer.reset(nullptr);
    keyboardState = SDL_GetKeyboardState(NULL);
}

Game::~Game()
{
}

GLuint Game::elapsedTicks()
{
    return SDL_GetTicks()-m_startTick;
}

bool Game::init()
{
    using namespace std;

    SDL_Log("INIT CALLED");

    //////////Init SDL2
    if (SDL_Init(SDL_INIT_EVERYTHING))
    {
        cout<<"SDL did not initialize correctly"<<endl;
        return true;
    }

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, SDL_TRUE);

    //This makes our buffer swap syncronized with the monitor's vertical refresh
    SDL_GL_SetSwapInterval(1);

    //Create a render object
    SDL_Log("Creating renderer");
    m_renderer.reset(new Renderer());

    //create a new world
    SDL_Log("Creating world");
    m_world.reset(new World());

    //////////Init OpenGL
    glEnable(GL_DEPTH_TEST);

    //share the new world with the rendering object so that it knows what to render
    //sharing the pointer is better than passing it over every frame
    m_renderer->setWorld(&m_world);

    // tell stb_image.h to flip loaded texture's on the y-axis (before loading model).
    stbi_set_flip_vertically_on_load(true);

    //set it to be a test world
    m_world->testWorld2();

    m_renderer->loadTextures();

    //set the camera to the first camera inserted into the world
    m_renderer->useFirstCamera();


    return false;
}

bool Game::startGame()
{
    if (start())
    {
        std::cout<<"Game did not start correctly"<<std::endl;
        return true;
    }
    return false;
}

bool Game::start()
{
    if (isRunning)
    {
        return true;
    }

    isRunning = true;
    m_startTick=SDL_GetTicks();

    run();

    return false;
}

bool Game::stop()
{
    if (!isRunning)
    {
        return true;
    }

    isRunning = false;

    // Shutdown SDL 2
    SDL_Quit();

    return false;
}

bool Game::run()
{
    using namespace std;

    bool newFrame=true;

    bool printCamCoords=false;
    bool updateViewBox=false;

    bool leftMousePressed=false;
    bool leftShiftPressed=false;

    bool shouldStop = false;

    GLuint previousStart = 0;
    GLuint currentStart = 0;
    GLuint deltaTime = 0;
    float seconds = 0.0f;

    SDL_Event event;
    while (isRunning)
    {
        //Update how many ticks has passed since the last iteration
        currentStart = elapsedTicks();
        deltaTime = currentStart-previousStart;
        previousStart=currentStart;

        seconds = static_cast<float>(deltaTime)/1000.0f;

//        keyboardState = SDL_GetKeyboardState(NULL);

        if (keyboardState[SDL_SCANCODE_Z]) {
            printf("z is down.\n");
        }

        //is called once on every frame
        //TODO: Remove modulus and use > instead. This is in case of very low FPS might 'jump over' the %check
        if (elapsedTicks()%TICKS_PER_FRAME==0)
        {
            if (newFrame || true)
            {
                m_elapsedFrames++;

                /*
                if (m_elapsedFrames%10)
                    m_renderer->render2();
                else
                    m_renderer->render();
                */

                m_renderer->render2();

                newFrame=false;
//                cout<<m_elapsedFrames<<endl;
            }
        }
        else
        {
            newFrame=true;
        }

        GLfloat velocity = 1.0f;
        if (keyboardState[SDL_SCANCODE_LSHIFT])
        {
            velocity*=10;
        }

        while (SDL_PollEvent(&event))
        {
            if (event.type==SDL_QUIT)
            {
                shouldStop=true;
            }

            if (event.type==SDL_WINDOWEVENT)
            {
                switch (event.window.event)
                {
                case SDL_WINDOWEVENT_RESIZED:
                {
                    m_renderer->resize(event.window.data1, event.window.data2);
                    break;
                }
                }
            }

            if (event.type==SDL_MOUSEWHEEL)
            {
                m_renderer->mouseScroll(event.wheel.x, event.wheel.y);
                printCamCoords=true;
                updateViewBox=true;
            }

            if (event.type==SDL_MOUSEBUTTONDOWN)
            {
                switch (event.button.button)
                {
                case SDL_BUTTON_LEFT:
                {
                    leftMousePressed=true;
                    SDL_Log("left down");
                    break;
                }
                case SDL_BUTTON_RIGHT:
                {
                    SDL_Log("Right mouse clicked at x%i, y%i", event.button.x, event.button.y);
                }
                }
            }

            if (event.type==SDL_MOUSEBUTTONUP)
            {
                switch (event.button.button)
                {
                case SDL_BUTTON_LEFT:
                {
                    leftMousePressed=false;
                    SDL_Log("left up");
                }
                }
            }

            if (event.type==SDL_MOUSEMOTION)
            {
                //negate the y movement since SDL uses flipped y axis
                m_renderer->mouseMotion(event.motion.xrel, -event.motion.yrel);

                printCamCoords=true;
            }

            if (event.type==SDL_KEYDOWN)
            {
                switch (event.key.keysym.sym) {
                case SDLK_ESCAPE:
                {
                    shouldStop=true;
                    break;
                }
                case SDLK_r:
                {
                    m_renderer->flipWireframe();
                    break;
                }

                }
            }

            if (event.type==SDL_KEYUP)
            {
                switch (event.key.keysym.sym) {
                }
            }
        }

        if (keyboardState[SDL_SCANCODE_W])
        {
            m_renderer->moveCamera(Camera::FORWARD, velocity*seconds);
            printCamCoords=true;
        }

        if (keyboardState[SDL_SCANCODE_S])
        {
            m_renderer->moveCamera(Camera::BACKWARD, velocity*seconds);
            printCamCoords=true;
        }

        if (keyboardState[SDL_SCANCODE_A])
        {
            m_renderer->moveCamera(Camera::LEFT, velocity*seconds);
            printCamCoords=true;
        }

        if (keyboardState[SDL_SCANCODE_D])
        {
            m_renderer->moveCamera(Camera::RIGHT, velocity*seconds);
            printCamCoords=true;
        }

        if (keyboardState[SDL_SCANCODE_Q])
        {
            m_renderer->moveCamera(Camera::DOWN, velocity*seconds);
            printCamCoords=true;
        }

        if (keyboardState[SDL_SCANCODE_E])
        {
            m_renderer->moveCamera(Camera::UP, velocity*seconds);
            printCamCoords=true;
        }

        if (printCamCoords)
        {
            //SDL_Log("Cam: %s", glm::to_string(m_renderer->getCameraPosition()).c_str());
            printCamCoords=false;
        }

        if (shouldStop)
        {
            std::cout<<std::endl;
            stop();
        }
    }

    return false;
}










































































































