#ifndef MODEL3D_H
#define MODEL3D_H

#include <vector>
#include <glm/glm.hpp>
#include <string>
#include <iostream>
#include "shader.h"
#include "stb_image.h"

struct Vertex {
    // position
    glm::vec3 Position;
    // normal
    glm::vec3 Normal;
    // texCoords
    glm::vec2 TexCoords;
    // tangent
    glm::vec3 Tangent;
    // bitangent
    glm::vec3 Bitangent;
};

struct Texture {
    GLuint id;
    std::string type;
    std::string path;
};

enum eTemplates {
    TRIANGLE=0,
    SQUARE,
    CIRCLE,
    CUBE
};

class Model3d
{
public:
    // mesh data
    //std::vector<Vertex> vertices;

    Model3d();

    GLuint VAO() const;
    void setVAO(const GLuint &VAO);

    GLuint VBO() const;
    void setVBO(const GLuint &VBO);

    void addVertex5(GLfloat x, GLfloat y, GLfloat z, GLfloat t0, GLfloat t1);

    std::vector<GLfloat> getVertices();
    std::vector<Texture> getTextures();

    void assignTexture(Texture texture);
    void addDesiredTexture(std::string path);

    void bindTextures();

    void activate();

private:

    //  render data
    GLuint m_VAO;
    GLuint m_VBO;
    GLuint m_EBO;

    std::vector<Texture> m_textures;
    std::vector<GLfloat> m_vertices;

    void setupMesh();

};

#endif // MODEL3D_H
